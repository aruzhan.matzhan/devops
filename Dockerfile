FROM ubuntu:latest
FROM nginx

COPY default.conf .

RUN apt install nginx
RUN apt install systemd
RUN systemctl restart nginx

EXPOSE 8082