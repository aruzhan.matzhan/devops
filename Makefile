build: 
	docker image build -t web .
run:
	docker run -it --rm -d -p 8082:80 --name web nginx
stop:
	docker stop web
remove:
	docker rm web

clean: stop remove

all: build run