Установка и настройка Nginx:
1. Переходим по ссылке https://nginx.org/en/linux_packages.html 
2. Находим и копируем команды которые предназначены для ОS Ubuntu.

HTML-страница с текстом "Hello, DevOps World!":
1. Создайте файл index.html
2. Поместите файл index.html в директорию  /var/www/html/ :
     $sudo cp index.html /var/www/html/
3. Настройка конфигурации Nginx:
     $sudo nano /etc/nginx/sites-available/default
4. Если порт является числом in range [0,1000], поменяйте на свободный порт.
Например: "listen 4005;"
5. Перезапуск Nginx: 
     $sudo systemctl restart nginx
6. Откройте веб-браузер и перейдите по адресу вашего сервера:
     http://localhost:4005/
